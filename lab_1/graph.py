class Graph:
    def __init__(self, node):
        self.node = node
        self.children = dict()

    def new_child(self, child, label):
        if child in self.children:
            self.children[child].add(label)
        else:
            self.children[child] = {label}


def generate_table(graph):
    table = dict()
    graph_table(graph, table, set())
    return table


def graph_table(graph, table, visited):
    visited.add(graph)
    table[graph.node] = {child.node: label for child, label in graph.children.items()}
    for child in graph.children.keys():
        if child not in visited:
            graph_table(child, table, visited)


def generate_graph(table, start):
    nodes = dict()
    for node, children in table.items():
        if node not in nodes:
            nodes[node] = Graph(node)
        for child_node, label in children.items():
            if child_node not in nodes:
                nodes[child_node] = Graph(child_node)
            nodes[node].children[nodes[child_node]] = label
    return nodes[start]
