import graph


def dfa_proceed(dfa, terminal_nodes, input_string):
    try:
        for sym in input_string:
            it = iter(dfa.children)
            child = next(it)
            while sym not in dfa.children[child]:
                child = next(it)
            dfa = child
    except StopIteration:
        return False
    return dfa.node in terminal_nodes


def minimize(dfa_table, start, terminal_nodes):
    dfa = graph.generate_graph(dfa_table, start)
    start, terminal_nodes = rename_dfa_nodes(dfa, terminal_nodes)
    dfa_table = graph.generate_table(dfa)
    marked = unequivalence_table(dfa_table, terminal_nodes)
    nodes = list(dfa_table.keys())
    for node1 in range(len(nodes)):
        for node2 in range(node1 + 1, len(nodes)):
            if not marked[node1][node2]:
                unite_nodes(dfa_table, node1, node2)
                if node2 in terminal_nodes:
                    terminal_nodes.remove(node2)
    return dfa_table, start, terminal_nodes


def unite_nodes(table, node1, node2):
    if node2 in table:
        table.pop(node2)
        for children in table.values():
            if node2 in children:
                if node1 in children:
                    children[node1].update(children[node2])
                else:
                    children[node1] = children[node2]
                children.pop(node2)



def rename_dfa_nodes(dfa, old_terminal_nodes):
    start = 0
    num = start
    queue = [dfa]
    new_terminal_nodes = set()
    visited = set()
    while len(queue):
        node = queue.pop(0)
        if node.node in old_terminal_nodes:
            new_terminal_nodes.add(num)
        node.node = num
        num += 1
        for child in node.children.keys():
            if child not in visited:
                queue.append(child)
                visited.add(child)
    return start, new_terminal_nodes


def unequivalence_table(dfa_table, terminal_nodes):
    pairs_queue = list()
    nodes_amount = len(dfa_table)
    marked = [[False] * nodes_amount for _ in range(nodes_amount)]
    for i in range(nodes_amount):
        for j in range(nodes_amount):
            if not marked[i][j] and ((i in terminal_nodes) != (j in terminal_nodes)):
                marked[i][j] = marked[j][i] = True
                pairs_queue.append((i, j))
    alphabet = alphabet_from_table(dfa_table)
    reverse = reverse_transitions(dfa_table)
    while len(pairs_queue):
        (node1, node2) = pairs_queue.pop(0)
        for sym in alphabet:
            if sym in reverse[node1]:
                for parent1 in reverse[node1][sym]:
                    if sym in reverse[node2]:
                        for parent2 in reverse[node2][sym]:
                            if not marked[parent1][parent2]:
                                marked[parent1][parent2] = marked[parent2][parent1] = True
                                pairs_queue.append((parent1, parent2))
    return marked


def reverse_transitions(dfa_table):
    res = dict()
    for node, children in dfa_table.items():
        if node not in res:
            res[node] = dict()
        for child, symbols in children.items():
            for sym in symbols:
                if child not in res:
                    res[child] = dict()
                if sym not in res[child]:
                    res[child][sym] = {node}
                else:
                    res[child][sym].add(node)
    return res


def NFA_table_to_DFA_table(nfa_table, start, nfa_terminal_nodes):
    queue = [{start}]
    dfa_nodes = set()
    dfa_table = dict()
    dfa_terminal_nodes = set()
    while len(queue):
        nodes = queue.pop(0)
        for node in nodes:
            if node in nfa_terminal_nodes:
                dfa_terminal_nodes.add(frozenset(nodes))
        symbols = set()
        for node in nodes:
            for syms in nfa_table[node].values():
                symbols.update(syms)
        for sym in symbols:
            child = set()
            for node in nodes:
                for child_node, child_sym in nfa_table[node].items():
                    if sym in child_sym:
                        child.add(child_node)
            child = frozenset(child)
            if frozenset(nodes) not in dfa_table:
                dfa_table[frozenset(nodes)] = dict()
            dfa_table[frozenset(nodes)][child] = {sym}
            if child not in dfa_nodes:
                queue.append(child)
                dfa_nodes.add(child)

    return dfa_table, frozenset({start}), dfa_terminal_nodes


def remove_empy_transitions(nfa, terminal_nodes):
    visited = {nfa}
    queue = [nfa]
    while len(queue):
        node = queue.pop(0)
        for child in list(node.children):
            offspring_queue = [child]
            while len(offspring_queue):
                descendant = offspring_queue.pop(0)
                if "eps" in node.children[descendant]:
                    if len(node.children[descendant]) == 1:
                        node.children.pop(descendant)
                    else:
                        node.children[descendant].remove("eps")
                    if descendant.node in terminal_nodes:
                        terminal_nodes.add(node.node)

                    for grandchild in descendant.children:
                        if grandchild in node.children:
                            node.children[grandchild].update(descendant.children[grandchild])
                        else:
                            node.children[grandchild] = descendant.children[grandchild]
                        offspring_queue.append(grandchild)
            if child not in visited:
                queue.append(child)
        visited.add(node)
    return refresh_terminal_nodes(nfa, terminal_nodes)


def refresh_terminal_nodes(fa, nodes):
    new_nodes = set()
    visited = {fa}
    queue = [fa]
    while len(queue):
        node = queue.pop(0)
        if node.node in nodes:
            new_nodes.add(node.node)
        for child in node.children:
            if child not in visited:
                queue.append(child)
        visited.add(node)
    return new_nodes


def alphabet_from_table(table):
    res = set()
    for col in table.values():
        for item in col.values():
            res.update(item)
    return res
