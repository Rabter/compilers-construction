class BinaryTree:
    def __init__(self, root_label):
        self.label = root_label
        self.left = None
        self.right = None
