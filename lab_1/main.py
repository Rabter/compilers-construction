import graph
import regex
import finite_automaton as fa


def print_table(table):
    col_keys = set()
    maxlen = max(map(len, map(str, table.keys())))
    for col in table.values():
        col_keys = col_keys.union(list(col.keys()))
        if len(col.values()):
            maxlen = max(maxlen, max(map(len, map(str, col.keys()))))
            maxlen = max(maxlen, max(map(len, map(str, col.values()))))
    maxlen += 2
    col_keys = sorted(list(col_keys))
    for header in [''] + col_keys:
        print(('{:^%d}' % maxlen).format(str(header)), end='')
    print()

    line_keys = table.keys()
    for line_key in sorted(line_keys):
        print(('{:^%d}' % maxlen).format(str(line_key)), end='')
        for col_key in col_keys:
            if col_key in table[line_key].keys():
                print(('{:^%d}' % maxlen).format(str(table[line_key][col_key])), end='')
            else:
                print(('{:^%d}' % maxlen).format(''), end='')
        print()


if __name__ == "__main__":
    regex1 = "b*"
    regex2 = "(a|b)*b"
    re = regex2
    ast = regex.generate_ast(re)
    nfa, start, end = regex.generate_nfa(ast)
    table = graph.generate_table(nfa)
    print("Start state:", start, "\nEnd state:", end)
    print("NFA Control table:\n")
    print_table(table)
    terminal_nodes = fa.remove_empy_transitions(nfa,  {end})
    table = graph.generate_table(nfa)
    print("\nStart state:", start, "\nTerminal states:", terminal_nodes)
    print("Reduced NFA Control table:\n")
    print_table(table)
    table, start, terminal_nodes = fa.NFA_table_to_DFA_table(table, start, terminal_nodes)
    print("\nStart state:", start, "\nTerminal states:", terminal_nodes)
    print("DFA Control table:\n")
    print_table(table)
    table, start, terminal_nodes = fa.minimize(table, start, terminal_nodes)
    print("\nStart state:", start, "\nTerminal states:", terminal_nodes)
    print("Minimized DFA Control table:\n")
    print_table(table)
    dfa = graph.generate_graph(table, start)
    print("Original regular expression:", re)
    string = input("Enter input string: ")
    print("DFA result:", fa.dfa_proceed(dfa, terminal_nodes, string))
