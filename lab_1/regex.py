from binary_tree import BinaryTree
from graph import Graph

unary_left = set("*")
binary = set("|.")
operators = list(".|*")


def generate_nfa(ast):
    start, end, _ = ast_to_nfa(ast, 0)
    return start, start.node, end.node


def ast_to_nfa(ast, nodenum):
    if ast.label in operators:
        left_start, left_end, nodenum = ast_to_nfa(ast.left, nodenum)
        if ast.label in binary:
            right_start, right_end, nodenum = ast_to_nfa(ast.right, nodenum)
        else:
            right_start = right_end = None
        return thompson(ast.label, left_start, left_end, right_start, right_end, nodenum)
    start = Graph(nodenum)
    end = Graph(nodenum + 1)
    start.new_child(end, ast.label)
    return start, end, nodenum + 2


def thompson(label, first_start, first_end, second_start, second_end, num):
    if label == '.':
        start = Graph(num)
        end = Graph(num + 1)
        start.new_child(first_start, "eps")
        first_end.new_child(second_start, "eps")
        second_end.new_child(end, "eps")
        return start, end, num + 2
    elif label == '|':
        start = Graph(num)
        end = Graph(num + 1)
        start.new_child(first_start, "eps")
        start.new_child(second_start, "eps")
        first_end.new_child(end, "eps")
        second_end.new_child(end, "eps")
        return start, end, num + 2
    elif label == '+':
        start = Graph(num)
        end = Graph(num + 1)
        start.new_child(first_start, "eps")
        first_end.new_child(first_start, "eps")
        first_end.new_child(end, "eps")
        return start, end, num + 2
    elif label == '*':
        start = Graph(num)
        end = Graph(num + 1)
        start.new_child(first_start, "eps")
        start.new_child(end, "eps")
        first_end.new_child(first_start, "eps")
        first_end.new_child(end, "eps")
        return start, end, num + 2
    else:
        raise ValueError


def generate_ast(regex):
    nodes = split_regex(regex)
    return nodes_to_tree(nodes)


def nodes_to_tree(nodes):
    for op in operators:
        if op in nodes:
            i = nodes.index(op)
            root = BinaryTree(nodes[i])
            root.left = nodes_to_tree(nodes[:i])
            if op not in unary_left:
                root.right = nodes_to_tree(nodes[i + 1:])
            return root
    if len(nodes) != 1:
        raise ValueError
    regex = nodes[0]
    if len(regex) == 1:
        return BinaryTree(regex)
    nodes = split_regex(regex)
    return nodes_to_tree(nodes)


def split_regex(expr):
    elements = list()
    el, i = unbracket(expr, 0)
    elements.append(el)
    while i < len(expr):
        if not (expr[i] in operators or expr[i - 1] in binary):
            elements.append('.')
        el, i = unbracket(expr, i)
        elements.append(el)
    return elements


def unbracket(expr, start):
    expr = expr[start:]
    if expr[0] == '(':
        opened = 1
        i = 1
        while opened > 0:
            if i == len(expr):
                raise ValueError
            if expr[i] == '(':
                opened += 1
            elif expr[i] == ')':
                opened -= 1
            i += 1
        expr = expr[:i]
        end = start + len(expr)
        while expr[0] == '(' and expr[-1] == ')':
            expr = expr[1: -1]
        return expr, end
    return expr[0], start + 1
