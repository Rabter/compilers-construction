from re import finditer
from copy import deepcopy
from string import ascii_uppercase
import numpy as np


def solve_matrix_equation(left, mul1, mul2, augend):
    rules = dict()
    for i in range(len(left)):
        for j in range(len(left[i])):
            rules[left[i][j]] = deepcopy(set(augend[i][j]))
            for k in range(len(mul2)):
                for expr in mul1[i][k]:
                    rules[left[i][j]].add(expr + mul2[k][j])
    return rules


class Grammar:
    def __init__(self, filename):
        self.rules = dict()
        self.N = set()

        with open(filename) as file:
            lines = file.readlines()

        self.T = set(lines[0].split())
        for line in lines[1:-1]:
            sep = line.index(' ')
            left, right = line[:sep], line[sep + 1:-1]
            self.N.add(left)
            if left in self.rules:
                self.rules[left].add(right)
            else:
                self.rules[left] = {right}

        self.start = lines[-1].rstrip()

    def update_rules(self, new_rules):
        for key, rules in new_rules.items():
            if key in self.rules:
                self.rules[key].update(new_rules[key])
            else:
                self.rules[key] = new_rules[key]

    def to_GNF(self):
        A, rights = np.transpose(list(self.rules.items()))
        R = [[None] * len(self.rules) for _ in range(len(self.rules))]  # self.rules X self.rules - dimensional matrix
        B = deepcopy(rights)
        i = 0
        for N in A:
            j = 0
            for rule in rights:
                item = list()
                for subrule in rule:
                    if subrule.find(N) == 0:
                        item.append(subrule[len(N):])
                        B[j].remove(subrule)
                R[i][j] = item
                j += 1
            i += 1

        A = [A]
        B = [B]

        Q = list()
        taken = self.N | self.T
        it = iter(reversed(ascii_uppercase))
        for i in range(len(self.rules)):
            line = list()
            for _ in range(len(self.rules)):
                try:
                    sym = next(it)
                except StopIteration:
                    it = iter(reversed(ascii_uppercase))
                    sym = next(it)
                while sym in taken:
                    sym += "'"
                line.insert(0, sym)
                taken.add(sym)
            Q.insert(0, line)

        self.rules = solve_matrix_equation(A, B, Q, B)
        new_rules = solve_matrix_equation(Q, R, Q, R)
        for key, rule_set in new_rules.items():
            replacement = set()
            for rule in rule_set:
                i = 0
                it = iter(self.N)
                found = False
                while i < len(self.N) and not found:
                    i += 1
                    nonterm = next(it)
                    if rule.find(nonterm) == 0:
                        found = True
                        for produced_rule in self.rules[nonterm]:
                            replacement.add(produced_rule + rule[len(nonterm):])
                if not found:
                    replacement.add(rule)
            new_rules[key] = replacement

        discarded = True
        while discarded:
            discarded = False
            for n, rights in list(new_rules.items()):
                if len(rights) == 0:
                    new_rules.pop(n)
                    for rule_set in deepcopy(list(self.rules.values()) + list(new_rules.values())):
                        for rule in set(rule_set):
                            if rule.find(n) >= 0:
                                rule_set.discard(rule)
                                discarded = True

        self.update_rules(new_rules)
        #self.rules.update(new_rules)

        self.N = set(self.rules.keys())

        new_rules = dict()
        for rule_set in self.rules.values():
            for rule in set(rule_set):
                for term in self.T:
                    indicies = [m.start() for m in finditer(term, rule)]
                    if len(indicies) and indicies[0] == 0:
                        indicies = indicies[1:]
                    if len(indicies):
                        items = np.transpose(list(new_rules.items()))
                        if len(items) and {term} in items[1]:
                            replacement = items[0][list(items[1]).index({term})]
                        else:
                            taken = self.T | self.N | set(new_rules.keys())
                            replacement = term + "'"
                            while replacement in taken:
                                replacement += "'"
                            new_rules[replacement] = {term}
                        for i in indicies:
                            rule_set.discard(rule)
                            rule_set.add(rule[:i] + replacement + rule[i + len(term):])
       #self.rules.update(new_rules)
        self.update_rules(new_rules)

        self.N = set(self.rules.keys())

    def save(self, filename):
        with open(filename, 'w') as file:
            terms = ""
            for t in self.T:
                terms += t + ' '
            file.write(terms[:-1] + '\n')

            for n, rule_set in self.rules.items():
                for rule in rule_set:
                    file.write(n + ' ' + rule + '\n')

            file.write(self.start)

    def print(self):
        print("Terminals:", self.T)
        print("Nonterminals:", self.N)
        print("Start:", self.start)
        for n, rule_set in self.rules.items():
            print(n, end=" -> ")
            rules = ""
            for rule in rule_set:
                rules += rule + '|'
            print(rules[:-1])
