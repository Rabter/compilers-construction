from grammar import Grammar

if __name__ == "__main__":
    gram = Grammar(input("Enter grammar file name: "))
    print("Original grammar:")
    gram.print()
    gram.to_GNF()
    print("\nThe grammar in Greibach normal form:")
    gram.print()
    gram.save("GNF.txt")
