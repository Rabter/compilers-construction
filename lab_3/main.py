from string import ascii_lowercase


def sum_signs(string, i):
    signs = {'+', '-'}
    if len(string) > i and string[i] in signs:
        return [i + 1]
    else:
        return list()


def mul_signs(string, i):
    signs = {'*', '/', '%'}
    if len(string) > i and string[i] in signs:
        return [i + 1]
    else:
        return list()


def relation_signs(string, i):
    signs = {'<', '<=', '==', '>=', '>', '<>'}
    res = list()
    for rule in signs:
        if string.find(rule, i) == i:
            res.append(i + len(rule))
    return res


def digit(string, i):
    digits = {str(i) for i in range(10)}
    if len(string) > i and string[i] in digits:
        return [i + 1]
    else:
        return list()


def number(string, i):
    res = list()
    ilist = digit(string, i)
    if len(ilist):
        res.extend(ilist)
        ilist = digit(string, ilist[0])
        while len(ilist):
            res.extend(ilist)
            ilist = digit(string, ilist[0])
    return res


def letter(string, i):
    if i < len(string) and string[i].lower() in set(ascii_lowercase):
        return [i + 1]
    else:
        return list()


def identifier(string, i):
    res = list()
    ilist = letter(string, i)
    if len(ilist):
        while len(ilist):
            res.extend(ilist)
            let_list = letter(string, ilist[0])
            if len(let_list):
                ilist = let_list
            else:
                ilist = digit(string, ilist[0])
    return res


def primary_expression(string, i):
    res = list(set(number(string, i)) | set(identifier(string, i)))
    if string[i] == '(':
        for j in arithmetic_expression(string, i + 1):
            if string[j] == ')':
                res.append(j + 1)
    return res


def multiplier(string, i):
    res = list()
    for j in primary_expression(string, i):
        res.extend(modified_multiplier(string, j))
    return res


def modified_multiplier(string, i):
    res = [i]
    if i < len(string) and string[i] == '^':
        for j in primary_expression(string, i):
            res.extend(modified_multiplier(string, j))
    return res


def term(string, i):
    res = list()
    for j in multiplier(string, i):
        res.extend(modified_term(string, j))
    return res


def modified_term(string, i):
    res = [i]
    for j in mul_signs(string, i):
        for k in multiplier(string, j):
            res.extend(modified_term(string, k))
    return res


def arithmetic_expression(string, i):
    res = list()
    for j in term(string, i):
        res.extend(modified_arithmetic_expression(string, j))
    for j in sum_signs(string, i):
        for k in term(string, j):
            res.extend(modified_arithmetic_expression(string, k))
    return res


def modified_arithmetic_expression(string, i):
    res = [i]
    for j in sum_signs(string, i):
        for k in term(string, j):
            res.extend(modified_arithmetic_expression(string, k))
    return res


def expression(string, i):
    res = list()
    for j in arithmetic_expression(string, i):
        for k in relation_signs(string, j):
            res.extend(arithmetic_expression(string, k))
    return res


def block(string, i):
    res = list()
    if string[i] == '{':
        for j in operators_list(string, i + 1):
            if string[j] == '}':
                res.append(j + 1)
    return res


def operators_list(string, i):
    res = list()
    for j in operator(string, i):
        res.extend(tail(string, j))
    return res


def operator(string, i):
    res = list()
    for j in identifier(string, i):
        if string[j] == '=':
            res.extend(expression(string, j + 1))
    res.extend(block(string, i))
    return res


def tail(string, i):
    res = [i]
    if string[i] == ';':
        for j in operator(string, i + 1):
            res.extend(tail(string, j))
    return res


def program(string):
    if len(expression(string, 0)):
        return True
    if len(block(string, 0)):
        return True
    return False


# The grammar is not LL(1) so each function returns a list of the ending to all acceptable rules
if __name__ == "__main__":
    print(program("{var=(a*5)<b%(a*(c%b));newvar=b==c}"))
