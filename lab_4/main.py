from data import matrix
from string import ascii_lowercase


def lexeme(string):
    if len(set(string.lower()) - (set(ascii_lowercase) | {str(a) for a in range(10)})) == 0:
        return "a"
    else:
        return string


def tokenize(string):
    res = list()
    i = j = 0
    identifiers = set(ascii_lowercase) | {str(a) for a in range(10)}
    while i < len(string):
        if string[i].lower() in ascii_lowercase:
            while i < len(string) and string[i].lower() in identifiers:
                i += 1
        elif string[i] in {str(a) for a in range(10)}:
            while i < len(string) and string[i] in {str(a) for a in range(10)}:
                i += 1
        elif i < len(string) - 1 and string[i:i + 2] in {'<=', '>=', '<>'}:
            i += 2
        else:
            i += 1
        res.append(string[j:i])
        j = i
    return res


def error_message(num):
    err = "unknown"
    if num == '1':
        err = "missing expression"
    elif num == '2':
        err = "expression starts with a right parenthesis"
    elif num == '3':
        err = "missing operator"
    elif num == '4':
        err = "missing right parenthesis"

    print("Error:", err, sep=' ')


if __name__ == "__main__":
    string = input('Enter an expression:\n')
    if len(string) == 0 or string[-1] != '$':
        string += '$'
    polish = ""
    stack = ['$']
    i = 0
    tokens = tokenize(string)
    no_err = True
    while len(stack) > 1 or tokens[i] != '$' and no_err:
        rel = matrix[lexeme(stack[-1])][lexeme(tokens[i])]
        if rel in {'<', '='}:
            stack.append(tokens[i])
            i += 1
        elif rel == '>':
            cond = True
            while cond:
                if stack[-1] not in {'(', ')'}:
                    polish += stack[-1]
                cond = matrix[lexeme(stack[-2])][lexeme(stack[-1])] != '<'
                stack.pop()
        else:
            error_message(rel)
            no_err = False

    if no_err:
        print("Reverse polish notation:", polish)
